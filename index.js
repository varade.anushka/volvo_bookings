const { ApolloServer } = require("apollo-server-express");
const express = require("express")
const http = require("http")

const { typeDefs } = require("./typeDefs");
const { resolvers } = require("./resolvers");

const PORT = "3000";

const server = new ApolloServer({ typeDefs, resolvers, playground: true });

const app = express();
app.use(express.json({}));

server.applyMiddleware({ app });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

const serverStarted = () => {
  console.log(`🚀 Server ready at ${PORT}`);
};

httpServer.listen(PORT, serverStarted);
