const vehicle = {
  //vin is id
  "4Y1SL65848Z411439": {
    make: "Honda",
    model: "Accord",
    custId: "7f8b941b-9055-4d98-b32e-3f90742fa607",
    bookingIds: ["4f5c9726-132f-42f4-be74-5443f4bb3b57"],
  },
};

module.exports = { vehicle };
