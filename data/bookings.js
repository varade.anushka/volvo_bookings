let bookings = {
    '4f5c9726-132f-42f4-be74-5443f4bb3b57': {
    date: "12/12/2021",
    time: 9,
    details: {
      custId: "7f8b941b-9055-4d98-b32e-3f90742fa607",
      vin: "4Y1SL65848Z411439",
    },
  },
};

let timeDb = {
  "12/12/2021": {
    9: ["4f5c9726-132f-42f4-be74-5443f4bb3b57"],
  },
};

module.exports = { bookings, timeDb };
