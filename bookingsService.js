const { gql } = require("apollo-server-express");
const { v4: uuidv4 } = require("uuid");
const validator = require("email-validator");

let { bookings, timeDb } = require("./data/bookings");
let { capacity } = require("./data/capacity");
let { customer } = require("./data/customer");
let { vehicle } = require("./data/vehicle");

const typeDefs = gql`
  type BookingCapacity {
    message: String
  }

  type BookingCreation {
    message: String
    bookingId: String
  }

  type Details {
    custId: String
    vin: String
  }

  type BookingRetrieval {
    date: String
    time: Int
    details: Details
  }

  type BookingDetails {
    date: String
    time: String
  }

  type BookingByVIN {
    make: String
    model: String
    custId: String
    bookings: [BookingDetails]
  }

  input Customer {
    name: String
    email: String
    phoneNumber: String
  }

  input Vehicle {
    make: String
    model: String
    vin: String
  }

  input Booking {
    date: String
    time: Int
  }

  extend type Query {
    bookingRetrieval(date: String): [BookingRetrieval]
    bookingByVIN(vin: String): BookingByVIN
  }

  extend type Mutation {
    bookingCapacity(capacity: Int): BookingCapacity
    bookingCreation(
      customer: Customer!
      vehicle: Vehicle!
      booking: Booking!
    ): BookingCreation
  }
`;

const resolvers = {
  Mutation: {
    bookingCapacity: (_, args) => {
      if (!args.capacity) {
        throw new Error("Missing capacity in request");
      }

      if (args.capacity) capacity = args.capacity;
      return { message: `Successfully updated capacity to ${capacity}` };
    },

    bookingCreation: (_, args) => {
      if (!args.customer || !args.vehicle || !args.booking) {
        throw new Error("Missing parameters in request");
      }

      let { name, email, phoneNumber } = args.customer;
      let { make, model, vin } = args.vehicle;
      let { date, time } = args.booking;

      let re = "[A-HJ-NPR-Z0-9]{17}";
      if (!vin.match(re)) throw new Error("Please enter a valid 17 digit VIN");

      if (!validator.validate(email))
        throw new Error("Please enter valid email address");

      if (phoneNumber.match(/\d/g).length !== 10)
        throw new Error("Please enter valid phone number");

      if (![9, 11, 1, 3].includes(time))
        throw new Error(
          "We only accept booking time for 9am, 11am, 1pm and 3pm."
        );

      if (
        !timeDb[date] ||
        !timeDb[date][time] ||
        timeDb[date][time]?.length < capacity
      ) {
        let custId;
        let bookingId;
        if (!timeDb[date]) {
          timeDb[date] = {};
          timeDb[date][time] = [];
        }
        if (!timeDb[date][time]) {
          timeDb[date][time] = [];
        }
        custId = uuidv4();
        bookingId = uuidv4();
        timeDb[date][time].push(bookingId);
        bookings[bookingId] = {
          date,
          time,
          details: {
            custId,
            vin,
          },
        };

        customer[custId]
          ? { ...customer[custId], ...{ name, email, phoneNumber } }
          : (customer[custId] = { name, email, phoneNumber });

        if (vehicle[vin]) {
          vehicle[vin].bookingIds.push(bookingId);
        } else {
          let bookingIds = [bookingId];
          vehicle[vin] = {
            make,
            model,
            custId,
            bookingIds,
          };
        }

        return { message: "Success", bookingId };
      }

      throw new Error(
        "Capacity exceeded for given date and time. Provide a different date and/or time."
      );
    },
  },

  Query: {
    bookingRetrieval: (_, args) => {
      const date = args?.date;
      if (!date) {
        throw new Error("Missing date in request");
      }

      if (timeDb[date]) {
        let bookingDetails = [];
        Object.keys(timeDb[date]).forEach((time) => {
          timeDb[date][time].forEach((id) => {
            bookingDetails.push(bookings[id]);
          });
        });

        return bookingDetails;
      }

      throw new Error(`No bookings available for the provided date - ${date}`);
    },

    bookingByVIN: (_, args) => {
      const vin = args?.vin;

      if (!vin) {
        throw new Error("Missing vin in request");
      }

      let re = "[A-HJ-NPR-Z0-9]{17}";
      if (!vin.match(re)) throw new Error("Please enter a valid 17 digit VIN");

      if (vehicle[vin]) {
        let bookingDetails = [];
        let { make, model, custId, bookingIds } = vehicle[vin];

        bookingIds.forEach((id) => {
          let { date, time } = bookings[id];

          bookingDetails.push({
            date,
            time,
          });
        });

        return {
          make,
          model,
          custId,
          bookings: bookingDetails,
        };
      }

      throw new Error(`No Bookings available for given VIN - ${vin}`);
    },
  },
};

module.exports = { typeDefs, resolvers };
