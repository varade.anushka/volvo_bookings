const { typeDefs: bookingsServiceTypeDef } = require("./bookingsService");

const { gql } = require("apollo-server-express");

const queryTypeDef = gql`
  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }

  schema {
    query: Query
    mutation: Mutation
  }
`;

const typeDefs = [queryTypeDef, bookingsServiceTypeDef];

module.exports = { typeDefs };
