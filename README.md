# Volvo_Bookings

## Description
This project gives you the basic operations to create and retrieve a booking for Volvo service customers.

As a Volvo employee, you can adjust the capacity for servicing at a given date and time. You can also create a booking for new or existing Volvo customer and retrieve the bookings based on a vehicle vin number or a particular day.

## APIs
This project implements 4 endpoints to execute and validate the service bookings.

1. **Booking Capacity:**
    - The aim of this api is to define the capacity for servicing a vehicle on a particular date at a particular time, considering the working hours of 9am - 5pm and every booking can be of 2 hours.
    - On success, a success message is provided as an output.

2. **Booking Creation:**
    - This api helps a user to create a service booking by providing the customer details, vehicle details and service date an time.
    - Date format: `12/20/2021` (String), Time format: `9` (Int).
    - The booking can be created only for 9am, 11am, 1pm or 3pm ensuring the working hours and 2 hours service slot.
    - On success, a success message along with the booking id is provided.

3. **Booking Retrieval:**
    - This api allows you to view the bookings for a particular day.
    - The output will display the date, time and respective customer details with customer id and vehicle VIN number.

4. **Booking By VIN:**
    - Using this api, a user can view all the bookings made for a particular vehicle by providing it's VIN number.
    - A valid VIN must be provided. For example, you can generate a valid VIN [here](https://vingenerator.org/).

## Data Files
1. **TimeDb:**
    - TimeDb lists down the bookingIds for a particular date based on time slots.
    - Date is the key.

2. **Bookings:**
    - The data in bookings.js defines the list of all the bookings and includes customerId and vehicle VIN number as data.
    - BookingId is the key which is generated using the uuid npm package.

3. **Customer:**
    - Customers.js file lists the customer details with name, email and phoneNumber. A validation check happens for email and phoneNumber when a customer provides input.
    - CustomerId is the key which is generated at runtime using the uuid npm package.

4. **Vehicle:**
    - Vehicle.js contains the vehicle details such as make, model and vin as mentioned while creating the booking.
    - VIN number is the key and a validation is added when user provides the input.
    - It also stores all the bookingIds created for the vehicle.

## Run the project
```
git clone {https/ssh}
cd Volvo_Bookings
npm run start
Go to: http://localhost:3000/graphql
run query or mutation
```

## Authors and acknowledgment
Anushka Varade

## Project status and future scope
Currently this project uses the local files as a NoSQL database, which has limited functionality (only CRUD operations) and gets reset when server re-starts. But for future scope I can connect with real database such as MongoDB to achieve some additional functionalities (read lock, indexing etc.). I can also put Caching for frequently called GET endpoints. 
Unit test cases can be added for better automated testing.

For `Date` and `Time` as input parameters to the GET or POST query, as of now I'm taking them as a string literal but for the real use case graphql scalar datatypes must be used to define the format for valid data and time.


## Sample Query for GraphQL Playground
```
# Write your query or mutation here
mutation createBooking(
  $capacity: Int
  $name: String
  $email: String
  $phoneNumber: String
  $make: String
  $model: String
  $vin: String
  $date: String
  $time: Int
) {
  bookingCapacity(capacity: $capacity) {
    message
  }

  bookingCreation(
    customer: { name: $name, email: $email, phoneNumber: $phoneNumber }
    vehicle: { make: $make, model: $model, vin: $vin }
    booking: { date: $date, time: $time }
  ) {
    message
    bookingId
  }
}

query getBooking($date: String, $vin: String) {
  bookingRetrieval(date: $date) {
    date
    time
    details {
      custId
      vin
    }
  }

  bookingByVIN(vin: $vin) {
    make
    model
    custId
    bookings {
      date
      time
    }
  }
}
```

**Query Variables**
```
{
  "date": "12/12/2021",
  "vin": "1FTEF14N5KNB30636",
  "capacity": 4,
  "name": "Sam", "email": "xyz@gmail.com", "phoneNumber": "1122334455",
  "make": "Nissan", "model": "Altima",
  "time": 11
}
```
